package DaoMYOM;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import Interfaces.CadastroGUI;
import Interfaces.PosGUI;
import Modelo.Cadastro;



public class Daodados {

	// Configura essas vari�veis de acordo com o seu banco
	private final String URL = "jdbc:mysql://localhost/mydb",
			USER = "root", 
			PASSWORD = "ifsuldeminas";

	private Connection con;
	private Statement comando;

	private void conectar() {
		try {
			con = ConFactory.conexao(URL, USER, PASSWORD, ConFactory.MYSQL);
			comando = con.createStatement();
			System.out.println("Conectado!");
		} catch (ClassNotFoundException e) {
			imprimeErro("Erro ao carregar o driver", e.getMessage());
		} catch (SQLException e) {
			imprimeErro("Erro ao conectar", e.getMessage());
		}
	}

	private void fechar() {
		try {
			comando.close();
			con.close();
			System.out.println("Conex�o Fechada");
		} catch (SQLException e) {
			imprimeErro("Erro ao fechar conex�o", e.getMessage());
		}
	}

	private void imprimeErro(String msg, String msgErro) {
		JOptionPane.showMessageDialog(null, msg, "Erro critico", 0);
		System.err.println(msg);
		System.out.println(msgErro);
		System.exit(0);
	}

	public void insere(Cadastro l){
		conectar();
		PreparedStatement insertdados = null;

		try {



			String sql = "INSERT INTO cadastro (nome, senha, cpf, datanasc, email, nomea) VALUES(?,?,?,?,?,?)";
			insertdados = con.prepareStatement(sql);
			insertdados.setString(1, l.getNome());
			insertdados.setString(2, l.getSenha());
			insertdados.setString(3, l.getCPF());
			insertdados.setString(4, l.getDatanasc());
			insertdados.setString(5, l.getEmail());
			insertdados.setString(6, l.getNomeA());
			

			int r=insertdados.executeUpdate();

			if(r > 0){
				//comando.executeUpdate(sql);
				System.out.println("Dados Inseridos!");
			}
		} catch (SQLException e) {
			imprimeErro("Erro ao inserir dados", e.getMessage());
		} finally {
			fechar();
		}
	}
	
	public boolean validaLogin(String email, String senha){
		boolean ok=false;
		conectar();
		PreparedStatement insertdados = null;
		ResultSet rs;
		try{
			
			String sql = "SELECT email, senha from cadastro WHERE email = '"+email+"' and senha = '"+senha+"'";
			insertdados = con.prepareStatement(sql);
			
			rs = insertdados.executeQuery();
			
			while(rs.next())
			{
				ok = true;
			}
			
		
	} catch (SQLException e) {
		imprimeErro("Erro ao tentar fazer login", e.getMessage());
	} finally {
		fechar();
	}
		
	return ok;	
				
		
		
	}
	}

