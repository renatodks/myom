package DaoMYOM;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import Modelo.Cadastro;
import Modelo.Letras;



public class Daoletras {

	// Configura essas vari�veis de acordo com o seu banco
	private final String URL = "jdbc:mysql://localhost/mydb",
			USER = "root", 
			PASSWORD = "ifsuldeminas";

	private Connection con;
	private Statement comando;

	private void conectar() {
		try {
			con = ConFactory.conexao(URL, USER, PASSWORD, ConFactory.MYSQL);
			comando = con.createStatement();
			System.out.println("Conectado!");
		} catch (ClassNotFoundException e) {
			imprimeErro("Erro ao carregar o driver", e.getMessage());
		} catch (SQLException e) {
			imprimeErro("Erro ao conectar", e.getMessage());
		}
	}

	private void fechar() {
		try {
			comando.close();
			con.close();
			System.out.println("Conex�o Fechada");
		} catch (SQLException e) {
			imprimeErro("Erro ao fechar conex�o", e.getMessage());
		}
	}

	private void imprimeErro(String msg, String msgErro) {
		JOptionPane.showMessageDialog(null, msg, "Erro critico", 0);
		System.err.println(msg);
		System.out.println(msgErro);
		System.exit(0);
	}

	public void insere(Letras c){
		conectar();
		PreparedStatement insertdados = null;

		try {
			
			String sql = "INSERT INTO composi��o VALUES(?,?,?,?)";
			insertdados = con.prepareStatement(sql);
			
			
			insertdados.setString(1, c.getLetra());
			insertdados.setInt(2, c.getId_musica());
			insertdados.setString(3, c.getNomemusica());
			insertdados.setString(4, c.getCompositor());

			int r=insertdados.executeUpdate();

			if(r > 0){
				//comando.executeUpdate(sql);
				System.out.println("Letra Inserida!");
			}
		} catch (SQLException e) {
			imprimeErro("Erro ao inserir Letra", e.getMessage());
		} finally {
			fechar();
		}
	}

}