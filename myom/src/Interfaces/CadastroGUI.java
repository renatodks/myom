package Interfaces;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JRadioButton;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import DaoMYOM.Daodados;
import Modelo.Cadastro;





public class CadastroGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtnome;
	private JTextField txtnomea;
	private JTextField txtdata;
	private JTextField txtcpf;
	private JTextField txtemail;
	private JPasswordField txtsenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroGUI frame = new CadastroGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
					
						} 
				}
			
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroGUI() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setOpaque(true);
		setBounds(100, 100, 800, 600);

		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		setContentPane(contentPane);
		
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("M.Y.O.M.");
		lblNewLabel.setForeground(Color.LIGHT_GRAY);
		lblNewLabel.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 70));
		lblNewLabel.setBounds(0, 0, 358, 93);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Cadastro");
		lblNewLabel_1.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 30));
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setBounds(76, 145, 170, 43);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Make Your Own Music");
		lblNewLabel_2.setForeground(Color.LIGHT_GRAY);
		lblNewLabel_2.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel_2.setBounds(53, 85, 286, 35);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Nome");
		lblNewLabel_3.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel_3.setForeground(Color.LIGHT_GRAY);
		lblNewLabel_3.setBounds(10, 199, 78, 16);
		contentPane.add(lblNewLabel_3);
		
		txtnome = new JTextField();
		txtnome.setHorizontalAlignment(SwingConstants.LEFT);
		txtnome.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		txtnome.setBounds(10, 226, 281, 28);
		contentPane.add(txtnome);
		txtnome.setColumns(10);
		
		JLabel txtArtistico = new JLabel("Nome Artistico");
		txtArtistico.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		txtArtistico.setForeground(Color.LIGHT_GRAY);
		txtArtistico.setBounds(10, 277, 170, 16);
		contentPane.add(txtArtistico);
		
		JLabel lblNewLabel_4 = new JLabel("Data de Nascimento");
		lblNewLabel_4.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel_4.setForeground(Color.LIGHT_GRAY);
		lblNewLabel_4.setBounds(10, 349, 224, 16);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("E-mail");
		lblNewLabel_5.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel_5.setForeground(Color.LIGHT_GRAY);
		lblNewLabel_5.setBounds(563, 94, 78, 16);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("CPF");
		lblNewLabel_6.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel_6.setForeground(Color.LIGHT_GRAY);
		lblNewLabel_6.setBounds(563, 161, 78, 16);
		contentPane.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Senha");
		lblNewLabel_7.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel_7.setForeground(Color.LIGHT_GRAY);
		lblNewLabel_7.setBounds(563, 235, 78, 16);
		contentPane.add(lblNewLabel_7);
		
		JLabel lblNewLabel_8 = new JLabel("Sexo");
		lblNewLabel_8.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel_8.setForeground(Color.LIGHT_GRAY);
		lblNewLabel_8.setBounds(47, 415, 78, 14);
		contentPane.add(lblNewLabel_8);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Masculino");
		rdbtnNewRadioButton.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		rdbtnNewRadioButton.setForeground(Color.BLACK);
		rdbtnNewRadioButton.setBackground(Color.WHITE);
		rdbtnNewRadioButton.setBounds(10, 436, 144, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Feminino");
		rdbtnNewRadioButton_1.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		rdbtnNewRadioButton_1.setForeground(Color.BLACK);
		rdbtnNewRadioButton_1.setBackground(Color.WHITE);
		rdbtnNewRadioButton_1.setBounds(10, 462, 144, 23);
		contentPane.add(rdbtnNewRadioButton_1);
		
		JPanel panel = new JPanel();
		panel.setBounds(474, 349, 269, 177);
		contentPane.add(panel);
		
		JButton btnAdicionarFoto = new JButton("Adicionar Foto");
		btnAdicionarFoto.setForeground(Color.BLACK);
		btnAdicionarFoto.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		btnAdicionarFoto.setBackground(Color.WHITE);
		btnAdicionarFoto.setBounds(474, 310, 224, 26);
		contentPane.add(btnAdicionarFoto);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//Recupera da interface Gráfica
				String Nome = txtnome.getText();
				String NomeA = txtnomea.getText();
				String Senha = txtsenha.getText();
				String Email = txtemail.getText();
				String CPF = txtcpf.getText();
				String Datanasc = txtdata.getText();	
				
				
				Cadastro l = new Cadastro();			
				l.setNome(Nome);
				l.setNomeA(NomeA);
				l.setEmail(Email);
				l.setSenha(Senha);
				l.setCPF(CPF);
				l.setDatanasc(Datanasc);
				
				Daodados gravar = new Daodados();
				
				gravar.insere(l);
				
				JOptionPane.showMessageDialog(null, "Cadastrado Com Sucesso!");
				
				
				
			}
		});
		btnCadastrar.setForeground(Color.BLACK);
		btnCadastrar.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		btnCadastrar.setBackground(Color.WHITE);
		btnCadastrar.setBounds(474, 537, 180, 23);
		contentPane.add(btnCadastrar);
		
		txtnomea = new JTextField();
		txtnomea.setHorizontalAlignment(SwingConstants.LEFT);
		txtnomea.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		txtnomea.setColumns(10);
		txtnomea.setBounds(10, 304, 281, 28);
		contentPane.add(txtnomea);
		
		txtdata = new JTextField();
		txtdata.setHorizontalAlignment(SwingConstants.LEFT);
		txtdata.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		txtdata.setColumns(10);
		txtdata.setBounds(10, 376, 281, 28);
		contentPane.add(txtdata);
		
		txtcpf = new JTextField();
		txtcpf.setHorizontalAlignment(SwingConstants.LEFT);
		txtcpf.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		txtcpf.setColumns(10);
		txtcpf.setBounds(462, 199, 281, 28);
		contentPane.add(txtcpf);
		
		txtemail = new JTextField();
		txtemail.setHorizontalAlignment(SwingConstants.LEFT);
		txtemail.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		txtemail.setColumns(10);
		txtemail.setBounds(462, 121, 281, 28);
		contentPane.add(txtemail);
		
		txtsenha = new JPasswordField();
		txtsenha.setBounds(461, 260, 282, 33);
		contentPane.add(txtsenha);
		
		JLabel lblNewLabel_9 = new JLabel("New label");
		lblNewLabel_9.setIcon(new ImageIcon(CadastroGUI.class.getResource("/BackGroud/Untitled.jpg")));
		lblNewLabel_9.setBounds(0, 0, 784, 562);
		contentPane.add(lblNewLabel_9);
	}
}
