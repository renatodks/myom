package Interfaces;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;

import DaoMYOM.Daodados;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class InicioGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTable table;
	private JTable table_1;
	private JTextField txtlogin;
	private JPasswordField txtsenhacorreta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InicioGUI frame = new InicioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InicioGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("M.Y.O.M.");
		label.setForeground(Color.LIGHT_GRAY);
		label.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 70));
		label.setBounds(0, 0, 358, 93);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("Make Your Own Music");
		label_1.setForeground(Color.LIGHT_GRAY);
		label_1.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		label_1.setBounds(53, 85, 286, 35);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("Pesquisar M\u00FAsicas");
		label_2.setForeground(Color.LIGHT_GRAY);
		label_2.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		label_2.setBounds(10, 131, 178, 28);
		contentPane.add(label_2);
		
		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.LEFT);
		textField.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		textField.setColumns(10);
		textField.setBounds(10, 170, 286, 28);
		contentPane.add(textField);
		
		table = new JTable();
		table.setBorder(new LineBorder(null, 3, true));
		table.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		table.setBounds(10, 219, 286, 332);
		contentPane.add(table);
		
		JLabel lblPlaylists = new JLabel("PlayLists");
		lblPlaylists.setForeground(Color.LIGHT_GRAY);
		lblPlaylists.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		lblPlaylists.setBounds(413, 180, 95, 28);
		contentPane.add(lblPlaylists);
		
		table_1 = new JTable();
		table_1.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		table_1.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		table_1.setBounds(413, 219, 361, 332);
		contentPane.add(table_1);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				Daodados gravar = new Daodados();
	
				if(gravar.validaLogin(txtlogin.getText(), txtsenhacorreta.getText()))
				{
					PosGUI cadastro= new PosGUI();
					cadastro.setVisible(true);	
				
				}
				else
				{
					JOptionPane.showMessageDialog(null, "email ou senha inválidos", "Erro critico", 0);
				}
			
				
				
				
				
				
			}
		});
		btnEntrar.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		btnEntrar.setBounds(624, 149, 150, 23);
		contentPane.add(btnEntrar);
		
		JButton btnCadastrese = new JButton("Cadastre-se");
		btnCadastrese.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				CadastroGUI cadastro= new CadastroGUI();
				cadastro.setVisible(true);
				
				
			}
		});
		btnCadastrese.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		btnCadastrese.setBounds(425, 151, 150, 23);
		contentPane.add(btnCadastrese);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setForeground(Color.LIGHT_GRAY);
		lblLogin.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 22));
		lblLogin.setBounds(576, 11, 61, 28);
		contentPane.add(lblLogin);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setForeground(Color.LIGHT_GRAY);
		lblSenha.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 22));
		lblSenha.setBounds(576, 88, 95, 28);
		contentPane.add(lblSenha);
		
		txtlogin = new JTextField();
		txtlogin.setHorizontalAlignment(SwingConstants.LEFT);
		txtlogin.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		txtlogin.setBounds(425, 50, 349, 23);
		contentPane.add(txtlogin);
		txtlogin.setColumns(10);
		
		txtsenhacorreta = new JPasswordField();
		txtsenhacorreta.setBounds(425, 115, 349, 23);
		contentPane.add(txtsenhacorreta);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(InicioGUI.class.getResource("/BackGroud/Untitled.jpg")));
		lblNewLabel.setBounds(0, 0, 784, 562);
		contentPane.add(lblNewLabel);
	}
}
