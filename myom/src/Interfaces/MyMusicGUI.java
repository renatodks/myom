package Interfaces;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;

public class MyMusicGUI extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTable table_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyMusicGUI frame = new MyMusicGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyMusicGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setForeground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMyom = new JLabel("M.Y.O.M.");
		lblMyom.setForeground(Color.LIGHT_GRAY);
		lblMyom.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 70));
		lblMyom.setBounds(0, 0, 358, 93);
		contentPane.add(lblMyom);
		
		JLabel label_1 = new JLabel("Make Your Own Music");
		label_1.setForeground(Color.LIGHT_GRAY);
		label_1.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		label_1.setBounds(53, 85, 286, 35);
		contentPane.add(label_1);
		
		JLabel lblMyMusics = new JLabel("My Musics");
		lblMyMusics.setForeground(Color.RED);
		lblMyMusics.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 32));
		lblMyMusics.setBounds(394, 58, 181, 43);
		contentPane.add(lblMyMusics);
		
		JButton btnSair = new JButton("Sair");
		btnSair.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		btnSair.setBounds(624, 72, 150, 23);
		contentPane.add(btnSair);
		
		JLabel label_2 = new JLabel("<Nome do Usu\u00E1rio>");
		label_2.setForeground(Color.LIGHT_GRAY);
		label_2.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 16));
		label_2.setBounds(593, 11, 181, 40);
		contentPane.add(label_2);
		
		JLabel lblListaDeMusicas = new JLabel("Lista de m\u00FAsicas autorais");
		lblListaDeMusicas.setForeground(Color.LIGHT_GRAY);
		lblListaDeMusicas.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 17));
		lblListaDeMusicas.setBounds(30, 136, 260, 40);
		contentPane.add(lblListaDeMusicas);
		
		JLabel lblMinhasPlaylists = new JLabel("Minhas PlayLists");
		lblMinhasPlaylists.setForeground(Color.LIGHT_GRAY);
		lblMinhasPlaylists.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 17));
		lblMinhasPlaylists.setBounds(409, 136, 166, 40);
		contentPane.add(lblMinhasPlaylists);
		
		table = new JTable();
		table.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		table.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		table.setBounds(30, 191, 298, 360);
		contentPane.add(table);
		
		table_1 = new JTable();
		table_1.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		table_1.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		table_1.setBounds(409, 191, 322, 360);
		contentPane.add(table_1);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(MyMusicGUI.class.getResource("/BackGroud/Untitled.jpg")));
		lblNewLabel.setBounds(0, 0, 784, 562);
		contentPane.add(lblNewLabel);
	}
}
