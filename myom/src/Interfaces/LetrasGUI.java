package Interfaces;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class LetrasGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LetrasGUI frame = new LetrasGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LetrasGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMyon = new JLabel("M.Y.O.M.");
		lblMyon.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 70));
		lblMyon.setForeground(Color.LIGHT_GRAY);
		lblMyon.setBounds(0, 0, 358, 93);
		contentPane.add(lblMyon);
		
		JLabel lblMakeYourOwn = new JLabel("Make Your Own Music");
		lblMakeYourOwn.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		lblMakeYourOwn.setForeground(Color.LIGHT_GRAY);
		lblMakeYourOwn.setBounds(53, 85, 286, 35);
		contentPane.add(lblMakeYourOwn);
		
		JLabel label = new JLabel("<nome do usu\u00E1rio>");
		label.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 16));
		label.setForeground(Color.LIGHT_GRAY);
		label.setBounds(593, 11, 181, 40);
		contentPane.add(label);
		
		JLabel lblPesquisarMsicas = new JLabel("Pesquisar M\u00FAsicas");
		lblPesquisarMsicas.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		lblPesquisarMsicas.setForeground(Color.LIGHT_GRAY);
		lblPesquisarMsicas.setBounds(20, 195, 178, 28);
		contentPane.add(lblPesquisarMsicas);
		
		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.LEFT);
		textField.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		textField.setBounds(20, 234, 286, 23);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblLetra = new JLabel("Letra");
		lblLetra.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 16));
		lblLetra.setForeground(Color.LIGHT_GRAY);
		lblLetra.setBounds(20, 274, 97, 23);
		contentPane.add(lblLetra);
		
		JPanel panel = new JPanel();
		panel.setBounds(20, 308, 733, 243);
		contentPane.add(panel);
		
		JLabel label_1 = new JLabel("<informa\u00E7\u00F5es sobre");
		label_1.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		label_1.setForeground(Color.LIGHT_GRAY);
		label_1.setBounds(397, 213, 189, 23);
		contentPane.add(label_1);
		
		JLabel lblArtistaOuBanda = new JLabel("artista ou banda e ");
		lblArtistaOuBanda.setForeground(Color.LIGHT_GRAY);
		lblArtistaOuBanda.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		lblArtistaOuBanda.setBounds(397, 234, 178, 23);
		contentPane.add(lblArtistaOuBanda);
		
		JLabel lblMsica = new JLabel("m\u00FAsica>");
		lblMsica.setForeground(Color.LIGHT_GRAY);
		lblMsica.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		lblMsica.setBounds(397, 258, 80, 14);
		contentPane.add(lblMsica);
		
		textField_1 = new JTextField();
		textField_1.setForeground(Color.BLACK);
		textField_1.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 12));
		textField_1.setText("      <Foto do artista>");
		textField_1.setBounds(624, 148, 127, 124);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblLetras = new JLabel("Letras");
		lblLetras.setForeground(Color.RED);
		lblLetras.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 30));
		lblLetras.setBounds(76, 145, 170, 43);
		contentPane.add(lblLetras);
		
		JButton button = new JButton("Sair");
		button.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		button.setBounds(624, 72, 150, 23);
		contentPane.add(button);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(LetrasGUI.class.getResource("/BackGroud/Untitled.jpg")));
		lblNewLabel.setBounds(0, 0, 784, 562);
		contentPane.add(lblNewLabel);
	}
}
