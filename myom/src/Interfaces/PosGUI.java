package Interfaces;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.border.LineBorder;

import DaoMYOM.Daodados;
import DaoMYOM.Daoletras;
import Modelo.Cadastro;
import Modelo.Letras;


public class PosGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTable table;
	private JTextField txtletra;
	private JTextField txtnumero;
	private JTextField txtcompositor;
	private JTextField txtNomeDaMusica;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PosGUI frame = new PosGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PosGUI() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		table = new JTable();
		table.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		table.setBounds(23, 188, 348, 363);
		contentPane.add(table);
		
		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.LEFT);
		textField.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		textField.setBounds(23, 152, 269, 25);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblMyom = new JLabel("M.Y.O.M.");
		lblMyom.setForeground(Color.LIGHT_GRAY);
		lblMyom.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 70));
		lblMyom.setBounds(0, 0, 358, 93);
		contentPane.add(lblMyom);
		
		JButton btnAdicionarPlaylist = new JButton("Adicionar a Playlist");
		btnAdicionarPlaylist.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 14));
		btnAdicionarPlaylist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAdicionarPlaylist.setBounds(461, 491, 244, 23);
		contentPane.add(btnAdicionarPlaylist);
		
		JButton btnAdicionarAMy = new JButton("Adicionar a My Musics");
		btnAdicionarAMy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String letra = txtletra.getText();
			
				
				
				Letras c = new Letras();			
				
				c.setLetra(letra);
				
				Daoletras gravar = new Daoletras();
				
				gravar.insere(c);
				
				JOptionPane.showMessageDialog(null, "Letra salva!");
				
				
				
				
				
				
			}
		});
		btnAdicionarAMy.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 14));
		btnAdicionarAMy.setBounds(461, 525, 244, 23);
		contentPane.add(btnAdicionarAMy);
		
		JButton btnSair = new JButton("Sair");
		btnSair.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		btnSair.setBounds(624, 72, 150, 23);
		contentPane.add(btnSair);
		
		JLabel lblMakeYourOwn = new JLabel("Make Your Own Music");
		lblMakeYourOwn.setForeground(Color.LIGHT_GRAY);
		lblMakeYourOwn.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 20));
		lblMakeYourOwn.setBounds(53, 85, 286, 35);
		contentPane.add(lblMakeYourOwn);
		
		JLabel lblPesquisar = new JLabel("Pesquisar");
		lblPesquisar.setForeground(Color.LIGHT_GRAY);
		lblPesquisar.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 16));
		lblPesquisar.setBounds(20, 117, 111, 34);
		contentPane.add(lblPesquisar);
		
		JLabel lblMusicaAutoral = new JLabel("Musica Autoral");
		lblMusicaAutoral.setForeground(Color.LIGHT_GRAY);
		lblMusicaAutoral.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 16));
		lblMusicaAutoral.setBounds(426, 92, 200, 23);
		contentPane.add(lblMusicaAutoral);
		
		JButton btnBusca = new JButton("Busca");
		btnBusca.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnBusca.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 15));
		btnBusca.setBounds(296, 153, 107, 24);
		contentPane.add(btnBusca);
		
		JLabel lblBemvindo = new JLabel("Bem-Vindo!");
		lblBemvindo.setForeground(Color.RED);
		lblBemvindo.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 30));
		lblBemvindo.setBounds(394, 58, 200, 43);
		contentPane.add(lblBemvindo);
		
		JLabel label = new JLabel("<Nome do Usu\u00E1rio>");
		label.setForeground(Color.LIGHT_GRAY);
		label.setFont(new Font("SketchFlow Print", Font.BOLD | Font.ITALIC, 16));
		label.setBounds(593, 11, 181, 40);
		contentPane.add(label);
		
		txtletra = new JTextField();
		txtletra.setHorizontalAlignment(SwingConstants.CENTER);
		txtletra.setText("Componha aqui!");
		txtletra.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 15));
		txtletra.setBounds(426, 221, 348, 259);
		contentPane.add(txtletra);
		txtletra.setColumns(10);
		
		txtnumero = new JTextField();
		txtnumero.setText("N\u00FAmero da Composi\u00E7\u00E3o");
		txtnumero.setForeground(Color.DARK_GRAY);
		txtnumero.setHorizontalAlignment(SwingConstants.LEFT);
		txtnumero.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		txtnumero.setColumns(10);
		txtnumero.setBounds(426, 121, 348, 25);
		contentPane.add(txtnumero);
		
		txtcompositor = new JTextField();
		txtcompositor.setText("Nome do Compositor");
		txtcompositor.setHorizontalAlignment(SwingConstants.LEFT);
		txtcompositor.setForeground(Color.DARK_GRAY);
		txtcompositor.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		txtcompositor.setColumns(10);
		txtcompositor.setBounds(426, 153, 348, 25);
		contentPane.add(txtcompositor);
		
		txtNomeDaMusica = new JTextField();
		txtNomeDaMusica.setText("Nome da Musica");
		txtNomeDaMusica.setHorizontalAlignment(SwingConstants.LEFT);
		txtNomeDaMusica.setForeground(Color.DARK_GRAY);
		txtNomeDaMusica.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		txtNomeDaMusica.setColumns(10);
		txtNomeDaMusica.setBounds(426, 185, 348, 25);
		contentPane.add(txtNomeDaMusica);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		lblNewLabel.setIcon(new ImageIcon(PosGUI.class.getResource("/BackGroud/Untitled.jpg")));
		lblNewLabel.setBounds(0, 0, 784, 562);
		contentPane.add(lblNewLabel);
	}
}
